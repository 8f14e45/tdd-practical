const { FoxGooseBagOfCorn } = require('../../src/katas');

describe('FoxGooseBagOfCorn', () => {
  let game;

  beforeEach(() => {
    game = new FoxGooseBagOfCorn();
  });

  describe('start with new game', () => {
    test('the boat on the left side of the river', () => {
      // arrange
      const result = game.leftSide;

      // act

      // assert
      expect(result).toEqual(true);
    });

    test('all players on the left side of the river', () => {
      // arrange
      const expected = ['Fox', 'Goose', 'Bag of Corn'];
      const result = game.left;

      // act

      // assert
      expect(new Set(result)).toEqual(new Set(expected));
    });

    test('no players on the other side', () => {
      // arrange
      const result = game.right;

      // act

      // assert
      expect(result).toHaveLength(0);
    });
  });

  describe('go to the other side of the river with no player', () => {
    test('the boat on the right side of the river', () => {
      // arrange

      // act
      const { leftSide: result } = game.move();

      // assert
      expect(result).toEqual(false);
    });

    test('all players on the left side of the river', () => {
      // arrange
      const players = ['Fox', 'Goose', 'Bag of Corn'];

      // act
      const { left: result } = game.move();

      // assert
      expect(new Set(result)).toEqual(new Set(players));
    });

    test('no player on the right side of the river', () => {
      // arrange

      // act
      const { right: result } = game.move();

      // assert
      expect(result).toHaveLength(0);
    });

    test('the game is over', () => {
      // arrange

      // act
      const result = game.move().check.bind(game);

      // assert
      expect(result).toThrow('Game over! You should not let "Fox" alone with "Goose".');
    });

    test('cannot have another move when the game is over', () => {
      // arrange

      // act
      const result = game.move().move.bind(game);

      // assert
      expect(result).toThrow('Game over! You should not let "Fox" alone with "Goose".');
    });
  });

  describe('take "Fox" over the river', () => {
    test('the boat on the right side of the river', () => {
      // arrange
      const player = 'Fox';

      // act
      const { leftSide: result } = game.move(player);

      // assert
      expect(result).toEqual(false);
    });

    test('no "Fox" on the left side of the river', () => {
      // arrange
      const player = 'Fox';

      // act
      const { left: result } = game.move(player);

      // assert
      expect(result).toEqual(expect.not.arrayContaining([player]));
    });

    test('"Fox" on the right side of the river', () => {
      // arrange
      const player = 'Fox';

      // act
      const { right: result } = game.move(player);

      // assert
      expect(result).toEqual([player]);
    });

    test('the game is over', () => {
      // arrange
      const player = 'Fox';

      // act
      const result = game.move(player).check.bind(game);

      // assert
      expect(result).toThrow('Game over! You should not let "Goose" alone with "Bag of Corn".');
    });
  });

  describe('take "Goose" over the river', () => {
    test('the boat on the right side of the river', () => {
      // arrange
      const player = 'Goose';

      // act
      const { leftSide: result } = game.move(player);

      // assert
      expect(result).toEqual(false);
    });
    
    test('"Goose" on the right side of the river', () => {
      // arrange
      const player = 'Goose';

      // act
      const { right: result } = game.move(player);

      // assert
      expect(result).toEqual([player]);
    });

    test('"Goose" is not on the left side of the river', () => {
      // arrange
      const player = 'Goose';

      // act
      const { left: result } = game.move(player);

      // assert
      expect(result).toEqual(expect.not.arrayContaining([player]));
    });

    test('check is the game can continue', () => {
      // arrange
      const player = 'Goose';

      // act
      const result = game.move(player).check.bind(game);

      // assert
      expect(result).not.toThrow();
    });
  });

  describe('go back after take "Goose" over the river', () => {
    const player = 'Goose';

    beforeEach(() => {
      game
        .move(player) // take "Goose" over
        .move();      // go back
    });
    
    test('the boat on the left side of the river', () => {
      // arrange
      const result = game.leftSide

      // act

      // assert
      expect(result).toEqual(true);
    });

    test('no "Goose" on the left side of the river', () => {
      // arrange
      const result = game.left;

      // act

      // assert
      expect(result).toEqual(expect.not.arrayContaining([player]));
    });

    test('"Goose" on the right side of the river', () => {
      // arrange
      const result = game.right;

      // act

      // assert
      expect(result).toEqual([player]);
    });

    test('check is the game can continue', () => {
      // arrange

      // act
      const result = game.check.bind(game);

      // assert
      expect(result).not.toThrow();
    });
  });

  describe('bring the player back from the right side of the river', () => {
    const player = 'Goose';

    beforeEach(() => {
      game
        .move(player)   // take "Goose" over
        .move(player);  // bring "Goose" back
    });
    
    test('the boat on the left side of the river', () => {
      // arrange
      const result = game.leftSide;

      // act

      // assert
      expect(result).toEqual(true);
    });

    test('all the players on the left side of the river', () => {
      // arrange
      const players = ['Fox', 'Goose', 'Bag of Corn'];
      const result = game.left;

      // act

      // assert
      expect(new Set(result)).toEqual(new Set(players));
    });

    test('no players on the right side of the river', () => {
      // arrange
      const result = game.right;

      // act

      // assert
      expect(result).toHaveLength(0);
    });
  });

  describe('take all players over the river successfully', () => {
    const players = ['Fox', 'Goose', 'Bag of Corn'];

    beforeEach(() => {
      game
        .move(players[1]) // take "Goose" over
        .move()           // go back
        .move(players[0]) // take "Fox" over
        .move(players[1]) // bring "Goose" back
        .move(players[2]) // take "Bag of Corn" over
        .move()           // go back
        .move(players[1]) // take "Goose" over
    });

    test('the boat on the right side of the river', () => {
      // arrange
      const result = game.leftSide;

      // act

      // assert
      expect(result).toEqual(false);
    });

    test('no players on the left side of the river', () => {
      // arrange
      const result = game.left;

      // act

      // assert
      expect(result).toHaveLength(0);
    });

    test('all the players on the right side of the river', () => {
      // arrange
      const result = game.right;

      // act

      // assert
      expect(new Set(result)).toEqual(new Set(players));
    });

    test('the game is finished', () => {
      // arrange

      // act
      const result = game.check.bind(game);

      // assert
      expect(result).toThrow('You have finished the game. Congratulations!');
    });

    test('cannot move after game is finished', () => {
      // arrange

      // act
      const result = game.move.bind(game);

      // assert
      expect(result).toThrow('You have finished the game. Congratulations!');
    });
  });

  describe('take "Goose", "Fox" and "Bag of Corn" over in turn', () => {
    const players = ['Fox', 'Goose', 'Bag of Corn'];

    beforeEach(() => {
      try {
        game
          .move(players[1]) // take "Goose" over
          .move()           // go back
          .move(players[0]) // take "Fox" over
          .move()           // go back <- FAIL
          .move(players[2]) // take "Bag of Corn" over
      } catch (error) {
      }
    });
    
    test('the game stop at fail step', () => {
      // arrange

      // act
      const result = game.check.bind(game);

      // assert
      expect(result).toThrow('Game over! You should not let "Goose" alone with "Fox".');
    });

    test('the boat on the left side of the river', () => {
      // arrange
      const result = game.leftSide;

      // act

      // assert
      expect(result).toEqual(true);
    });

    test('"Fox" and "Goose" on the right side of the river', () => {
      // arrange
      const expected = ['Fox', 'Goose'];
      const result = game.right;

      // act

      // assert
      expect(new Set(result)).toEqual(new Set(expected));
    });

    test('"Bag of Corn" still on the left side of the river', () => {
      // arrange
      const player = 'Bag of Corn';
      const result = game.left;

      // act

      // assert
      expect(result).toEqual([player]);
    });
  });

  describe('take a player does not exists from a side of the river', () => {
    test('take "Rabbit" over, throw error', () => {
      // arrange
      const player = 'Rabbit';

      // act
      const result = game.move.bind(game, player);

      // assert
      expect(result).toThrow('Invalid player. Choose one of "Fox", "Goose", "Bag of Corn".');
    });

    test('take "Goose" over and bring "Fox" back, throw error', () => {
      // arrange
      const player = ['Fox', 'Goose'];

      // act
      const result = game
        .move(player[1])              // take "Goose" over
        .move.bind(game, player[0]);  // bring "Fox" back

      // assert
      expect(result).toThrow('Invalid player. Choose one of "Goose".');
    });
  });
});
