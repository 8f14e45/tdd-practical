const { FizzBuzz } = require('../../src/katas');

describe('FizzBuzz', () => {
  describe('with 100 printed numbers with rules', () => {
    let fizzBuzz;
    let numbers;

    beforeAll(() => {
      fizzBuzz = new FizzBuzz();
      numbers = fizzBuzz.print();
    });
    
    test('print 100 numbers', () => {
      // arrange

      // act

      // assert
      expect(numbers).toHaveLength(100);
    });

    test('the 1st number is "1"', () => {
      // arrange

      // act

      // assert
      expect(numbers[0]).toEqual(1);
    });

    test('the 97th number is "97"', () => {
      // arrange

      // act

      // assert
      expect(numbers[96]).toEqual(97);
    });

    test('the 3rd number is "Fizz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[2]).toEqual('Fizz');
    });

    test('the 6th number is "Fizz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[5]).toEqual('Fizz');
    });

    test('the 5th number is "Buzz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[4]).toEqual('Buzz');
    });

    test('the 10th number is "Buzz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[9]).toEqual('Buzz');
    });

    test('the 15th number is "FizzBuzz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[14]).toEqual('FizzBuzz');
    });

    test('the 13th number is "Fizz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[12]).toEqual('Fizz');
    });

    test('the 52th number is "Buzz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[51]).toEqual('Buzz');
    });

    test('the 35th number is "Buzz"', () => {
      // arrange

      // act

      // assert
      expect(numbers[34]).toEqual('Buzz');
    });
  });
});
