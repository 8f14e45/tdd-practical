const { StringCalculator } = require('../../src/katas');

describe('StringCalculator', () => {
  let stringCalculator;

  beforeEach(() => {
    stringCalculator = new StringCalculator();
  });

  describe('with empty string', () => {
    test('given "", return 0', () => {
      // arrange
      const str = '';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(0);
    });
  });

  describe('with single positive number', () => {
    test('given "1", return 1', () => {
      // arrange
      const str = '1';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(1);
    });

    test('given "101", return 101', () => {
      // arrange
      const str = '101';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(101);
    });
  });

  describe('with multiple positive numbers', () => {
    test('given "1;2", return 3', () => {
      // arrange
      const str = '1;2';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(3);
    });

    test('given "1;;1", return 2', () => {
      // arrange
      const str = '1;;1';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(2);
    });
  });

  describe('with new lines between numbers', () => {
    test('given "1\\n2;3", return 6', () => {
      // arrange
      const str = '1\n2;3';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(6);
    });

    test('given "1;\\n", return 1', () => {
      // arrange
      const str = '1;\n';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(1);
    });
  });

  describe('with different delimiters', () => {
    test('given "//;\\n1;2", return 3', () => {
      // arrange
      const str = '//;\n1;2';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(3);
    });

    test('given "//,\\n1,2,3\\n4", return 10', () => {
      // arrange
      const str = '//,\n1,2,3\n4';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(10);
    });
  });

  describe('with negative number', () => {
    test('given "1;-2", throw an error', () => {
      // arrange
      const str = '1;-2'

      // act
      const result = stringCalculator.add.bind(stringCalculator, str);

      // assert
      expect(result).toThrow('Negatives not allowed.');
    });
  });

  describe('with numbers bigger than 1000', () => {
    test('given "2;1001", return 2', () => {
      // arrange
      const str = '2;1001';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(2);
    });

    test('given "2;1001;999", return 1001', () => {
      // arrange
      const str = '2;1001;999';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(1001);
    });
  });

  describe('with delimiter of any length', () => {
    test('given "//[***]\\n1***2***3", return 6', () => {
      // arrange
      const str = '//[***]\n1***2***3';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(6);
    });
  });

  describe('with multiple delimiters of any length', () => {
    test('given "//[*][%]\\n1*2%3", return 6', () => {
      // arrange
      const str = '//[*][%]\n1*2%3';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(6);
    });

    test('given "//[***][%%][####]\\n1***2%%3\\n####4", return 10', () => {
      // arrange
      const str = '//[***][%%][####]\n1***2%%3\n####4';

      // act
      const result = stringCalculator.add(str);

      // assert
      expect(result).toEqual(10);
    });
  });
});
