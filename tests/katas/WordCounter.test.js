const { WordCounter } = require('../../src/katas');

describe('WordCounter', () => {
  let wordCounter;

  beforeEach(() => {
    wordCounter = new WordCounter();
  });

  describe('with an empty phrase', () => {
    test('given "", return ""', () => {
      // arrange
      const phrase = '';
      const delimiters = [' '];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('');
    });
  });

  describe('with a single word', () => {
    test('given "boom", return as expected', () => {
      // arrange
      const phrase = 'boom';
      const delimiters = [' '];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('boom, 1');
    });

    test('given "boom ", return as expected', () => {
      // arrange
      const phrase = 'boom ';
      const delimiters = [' '];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('boom, 1');
    });
  });

  describe('with a space between words', () => {
    test('given "boom bang", return as expected', () => {
      // arrange
      const phrase = 'boom bang';
      const delimiters = [' '];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('boom, 1\nbang, 1');
    });

    test('given "boom bang boom", return as expected', () => {
      // arrange
      const phrase = 'boom bang boom';
      const delimiters = [' '];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('boom, 2\nbang, 1');
    });
  });

  describe('with difference delimiters', () => {
    test('given "boom, bang, boom", return as expected', () => {
      // arrange
      const phrase = 'boom, bang, boom';
      const delimiters = [','];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('boom, 2\nbang, 1');
    });

    test('given "boom bang, boom", return as expected', () => {
      // arrange
      const phrase = 'boom bang, boom';
      const delimiters = [' ', ','];

      // act
      const result = wordCounter.count(phrase, delimiters);

      // assert
      expect(result).toEqual('boom, 2\nbang, 1');
    });
  });
});
