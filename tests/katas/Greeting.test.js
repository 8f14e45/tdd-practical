const { Greeting } = require('../../src/katas');

describe('Greeting', () => {
  let greeting;

  beforeEach(() => {
    greeting = new Greeting();
  });
  
  describe('with a name', () => {
    test('given "Bob", return "Hello, Bob."', () => {
      // arrange
      const names = 'Bob';

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Bob.');
    });
  });

  describe('without any name', () => {
    test('given "", return "Hello, my friend."', () => {
      // arrange
      const names = '';

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, my friend.');
    });
  });

  describe('with name is all uppercase', () => {
    test('given "", return "Hello, my friend."', () => {
      // arrange
      const names = 'JERRY';

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('HELLO JERRY!');
    });
  });

  describe('with multiple names', () => {
    test('given "["Jill", "Jane"]", return "Hello, Jill and Jane."', () => {
      // arrange
      const names = ['Jill', 'Jane'];

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Jill and Jane.');
    });

    test('given "["Amy", "Brian", "charlotte"]", return "Hello, Amy, Brian, and Charlotte."', () => {
      // arrange
      const names = ['Amy', 'Brian', 'Charlotte'];

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Amy, Brian, and Charlotte.');
    });
  });

  describe('with mixing of normal and shouted names', () => {
    test('given "["Amy", "BRIAN", "Charlotte"]", return "Hello, Amy and Charlotte. AND HELLO BRIAN!"', () => {
      // arrange
      const names = ['Amy', 'BRIAN', 'Charlotte'];

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Amy and Charlotte. AND HELLO BRIAN!');
    });
  });

  describe('with any entries in name are a string containing a comma', () => {
    test('given "["Bob", "Charlie, Dianne"]", return "Hello, Bob, Charlie, and Dianne."', () => {
      // arrange
      const names = ['Bob', 'Charlie, Dianne'];

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Bob, Charlie, and Dianne.');
    });
  });

  describe('with any entries intentional commas', () => {
    test('given "["Bob", "\"Charlie, Dianne\""]", return "Hello, Bob, Charlie, and Dianne."', () => {
      // arrange
      const names = ['Bob', '"Charlie, Dianne"'];

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Bob and Charlie, Dianne.');
    });

    test('given "["Bob", "\"Charlie, Dianne\"", "BRIAN", "Jill, Jane"]", return "Hello, Bob, Charlie, and Dianne."', () => {
      // arrange
      const names = ['Bob', '"Charlie, Dianne"', 'BRIAN', 'Jill, Jane'];

      // act
      const result = greeting.greet(names);

      // assert
      expect(result).toEqual('Hello, Bob, Charlie, Dianne, Jill, and Jane. AND HELLO BRIAN!');
    });
  });
});
