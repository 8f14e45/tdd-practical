const { GreatestCommonDivisor } = require('../../src/katas');

describe('GreatestCommonDivisor', () => {
  let greatestCommonDivisor;

  beforeEach(() => {
    greatestCommonDivisor = new GreatestCommonDivisor();
  });

  describe('with one of the number is "0"', () => {
    test('given "0, 1", return 1', () => {
      // arrange
      const a = 0;
      const b = 1;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(1);
    });

    test('given "2, 0", return 2', () => {
      // arrange
      const a = 2;
      const b = 0;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(2);
    });
  });

  describe('with both of the number are "0"', () => {
    test('given "0, 0", throw an error', () => {
      // arrange
      const a = 0;
      const b = 0;

      // act
      const result = greatestCommonDivisor.process.bind(null, a, b);

      // assert
      expect(result).toThrow('Isn\'t defined.');
    });
  });

  describe('with two positive integer number but 0', () => {
    test('given "1, 2", return 1', () => {
      // arrange
      const a = 1;
      const b = 2;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(1);
    });

    test('given "2, 4", return 2', () => {
      // arrange
      const a = 2;
      const b = 4;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(2);
    });

    test('given "5, 6", return 1', () => {
      // arrange
      const a = 5;
      const b = 6;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(1);
    });

    test('given "8, 8", return 8', () => {
      // arrange
      const a = 8;
      const b = 8;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(8);
    });
  });

  describe('with negative integer number', () => {
    test('given "-8, 12", return 4', () => {
      // arrange
      const a = -8;
      const b = 12;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(4);
    });

    test('given "-2, -2", return 2', () => {
      // arrange
      const a = -2;
      const b = -2;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(2);
    });

    test('given "-11, -7", return 1', () => {
      // arrange
      const a = -11;
      const b = -7;

      // act
      const result = greatestCommonDivisor.process(a, b);

      // assert
      expect(result).toEqual(1);
    });
  });
});
