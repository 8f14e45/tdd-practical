class GreatestCommonDivisor {
  process(a: number, b: number): number {
    if (a === 0 && b === 0) {
      throw new Error('Isn\'t defined.');
    }

    const x = Math.abs(a);
    const y = Math.abs(b);
    let i = (x < y && x !== 0) || (y === 0) ? x : y;

    for (; i > 0; i -= 1) {
      if (x % i === 0 && y % i === 0) {
        return i;
      }
    }
  }
}

export { GreatestCommonDivisor };
