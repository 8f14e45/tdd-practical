type TPlayer = string;

type TRule = {
  [key: string]: TPlayer[],
};

enum EPlayer {
  fox = 'Fox',
  goose = 'Goose',
  bagOfCorn = 'Bag of Corn',
}

class FoxGooseBagOfCorn {
  private _PLAYERS: TPlayer[] = [
    EPlayer.fox,
    EPlayer.goose,
    EPlayer.bagOfCorn,
  ];
  private _RULE: TRule = {
    [EPlayer.fox]: [EPlayer.goose],
    [EPlayer.goose]: [EPlayer.fox, EPlayer.bagOfCorn],
    [EPlayer.bagOfCorn]: [EPlayer.goose],
  };
  private _LEFT: TPlayer[];
  private _RIGHT: TPlayer[];
  private _LEFT_SIDE: boolean;

  constructor() {
    this._LEFT = this._PLAYERS;
    this._RIGHT = [];
    this._LEFT_SIDE = true;
  }

  get left(): TPlayer[] { return this._LEFT; }
  get right(): TPlayer[] { return this._RIGHT; }
  get leftSide(): boolean { return this._LEFT_SIDE; }

  move(player?: TPlayer): this {
    // check is the game over
    this.check();

    // if player is passed, take them to the other side of the river
    if (player) {
      const [thisSide, otherSide] = this._LEFT_SIDE
        ? [this._RIGHT, this._LEFT]
        : [this._LEFT, this._RIGHT];

      if (otherSide.indexOf(player) < 0) {
        throw new Error(`Invalid player. Choose one of "${otherSide.join('", "')}".`);
      }

      thisSide.push(otherSide.splice(otherSide.indexOf(player), 1)[0]);
    }

    // change the position of the boat
    this._LEFT_SIDE = !this._LEFT_SIDE;

    return this;
  }

  check(): void {
    // if all the players are move to the other side of the river, the game is fulfilled.
    if (!this._LEFT.length) {
      throw new Error('You have finished the game. Congratulations!');
    }

    // check those players on the other side of the boat
    const players = this._LEFT_SIDE ? this._RIGHT : this._LEFT;

    players.forEach((player) => {
      if (this._RULE[player]) {
        const foe = this._RULE[player].find(e => players.indexOf(e) >= 0);

        if (foe) {
          throw new Error(`Game over! You should not let "${player}" alone with "${foe}".`);
        }
      }
    });
  }
}

export { FoxGooseBagOfCorn };
