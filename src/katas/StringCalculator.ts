class StringCalculator {
  split(str: string, delimiters: string[]): number[] {
    const spliter = delimiters
      .map(e => e.replace(/([\^\$\+\*\?\{\}\,\.\?\!\=\-])/g, '\\$1'))
      .join('|');
    const numbers = str.split(new RegExp(spliter)).map(Number);

    return numbers;
  }

  sum(numbers: number[]): number {
    return numbers.reduce((s, n) => s + n, 0);
  }

  add(str: string): number {
    // split new line into multiple lines
    const lines = str.split('\n');
    let delimiters = [';'];

    // set the delimiters if the first line is the definition of delimiter(begin with "//")
    // remove first line
    if (/^\/\//.test(lines[0])) {
      delimiters = lines.shift().slice(2).match(/[^\[\]]+/g);
    }

    // calculate total by adding total of each line
    const total = lines.reduce((sum, line) => {
      const numbers = this.split(line, delimiters).filter(n => n <= 1000);

      if (numbers.some(n => n < 0)) {
        throw new Error('Negatives not allowed.');
      }

      return sum + this.sum(numbers);
    }, 0); // tslint:disable-line align

    return total;
  }
}

export { StringCalculator };
