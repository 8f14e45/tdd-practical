class WordCounter {
  split(str: string, delimiters: string[]): string[] {
    const spliter = delimiters
      .map(e => e.replace(/([\^\$\+\*\?\{\}\,\.\?\!\=\-])/g, '\\$1'))
      .join('|');
    const words = str
      .split(new RegExp(spliter))
      .map(w => w.trim())
      .filter(w => !!w);

    return words;
  }

  count(phrase: string, delimiters: string[]): string {
    const counter = {};
    const words = this.split(phrase, delimiters);

    words.forEach(w => counter[w] = (counter[w] || 0) + 1);

    const wordCount = Object.keys(counter).map(w => `${w}, ${counter[w]}`).join('\n');

    return wordCount;
  }
}

export { WordCounter };
