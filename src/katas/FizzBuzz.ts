type TRule = {
  test: Function;
  value: any;
};

class FizzBuzz {
  private _RULES: TRule[];

  constructor() {
    this._RULES = [
      {
        test: n => n % 3 === 0 && n % 5 === 0,
        value: 'FizzBuzz',
      },
      {
        test: n => n % 5 === 0 || `${n}`.includes('5'),
        value: 'Buzz',
      },
      {
        test: n => n % 3 === 0 || `${n}`.includes('3'),
        value: 'Fizz',
      },
    ];
  }

  print(n: number = 100, rules: TRule[] = this._RULES): any[] {
    const numbers = Array(n).fill(null).map((e, i) => {
      const num = i + 1;
      const rule = rules.find(rule => rule.test(num));

      return rule ? rule.value : num;
    });

    return numbers;
  }
}

export { FizzBuzz };
