class Greeting {
  sayHi(names: string[]): string {
    return `Hello, ${this.join(names)}.`;
  }

  shout(names: string[]): string {
    return `HELLO ${this.join(names)}!`;
  }

  join(names: string[]): string {
    return names.length < 2
      ? `${names}`
      : `${names.slice(0, -1).join(', ')}`
        + `${names.length > 2 ? ',' : ''}`
        + ` and ${names.slice(-1)}`;
  }

  flat(arr: any[]): any[] {
    return arr.length
      ? [...[].concat(arr[0]), ...this.flat(arr.slice(1))]
      : [];
  }

  greet(str: string|string[]): string {
    // put all the names in array and remove empty names
    // split the the names by comma if not inside the quotes
    // trim all the names
    const names = this.flat([].concat(str)
      .filter(n => !!n)
      .map(e => /^".*"$/.test(e) ? e.slice(0, -1).slice(1) : e.split(',')))
      .map(e => e.trim());

    // greet "my friend" if don't have any names
    if (!names.length) {
      return this.sayHi(['my friend']);
    }

    // split normal names and uppercase names into two array
    const normalNames = names.filter(n => n !== n.toUpperCase());
    const shoutedNames = names.filter(n => n === n.toUpperCase());

    // combination of normal and shouted names
    if (normalNames.length && shoutedNames.length) {
      return `${this.sayHi(normalNames)} AND ${this.shout(shoutedNames)}`;
    }

    // only shouted names
    if (shoutedNames.length) {
      return this.shout(shoutedNames);
    }

    // only normal names
    return this.sayHi(normalNames);
  }
}

export { Greeting };
